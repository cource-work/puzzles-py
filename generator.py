import random

from models import MyRequest
from processData import Matrix


def generateData(max_id):
    n = random.randint(2, 10)
    m = random.randint(2, 10)
    sumOfValues = n * m
    digits = []
    matrix = [[0 for _ in range(m)] for _ in range(n)]

    while sumOfValues > 0:
        i = random.randint(0, n - 1)
        j = random.randint(0, m - 1)
        value = random.randint(1, sumOfValues // 2 + 1)
        if matrix[i][j] == 0:
            valid_value = True
            if i > 0 and matrix[i - 1][j] == value:
                valid_value = False
            if i < n - 1 and matrix[i + 1][j] == value:
                valid_value = False
            if j > 0 and matrix[i][j - 1] == value:
                valid_value = False
            if j < m - 1 and matrix[i][j + 1] == value:
                valid_value = False
            if valid_value:
                digits.append({"i": i, "j": j, "value": value})
                matrix[i][j] = value
                sumOfValues -= value

    matrix = Matrix()
    request = MyRequest(n=n, m=m, digits=digits, conditions=[])
    if matrix.initializeVariables(request):
        _, result = matrix.processData()
    else:
        result = []
    reason = matrix.reason


    return {
        "n": n,
        "m": m,
        "digits": sorted(digits, key=lambda x: (x['i'], x['j'])),
        "conditions": [],
        "id_item": max_id + 1,
        "solving": result,
        "reason": reason
    }
