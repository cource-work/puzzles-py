import requests
import json
from generator import generateData


def send_request(data):
    url = "http://127.0.0.1:5049/process_data/"
    headers = {"Content-Type": "application/json"}

    try:
        response = requests.post(url, headers=headers, data=json.dumps(data))

        if response.status_code == 200:
            result = response.json()
            print("Отриманий результат:")
            print(json.dumps(result, indent=4))
        else:
            print(f"Помилка при отриманні відповіді: {response.status_code}")

    except Exception as e:
        print(f"Сталася помилка: {e}")


if __name__ == "__main__":
    # Приклад умов задачі
    data = generateData()

    print(data)
    send_request(data)
