pipeline {
    agent any

    environment {
        // Встановіть необхідні змінні середовища, якщо потрібно
        GIT_CREDENTIALS_ID = 'creds'  // ID облікових даних GitLab у Jenkins
        REPO_URL = 'https://gitlab.com/cource-work/puzzles-py'
    }

    stages {
        stage('Checkout') {
            steps {
                script {
                    // Клонування репозиторію з GitLab
                    git url: REPO_URL, credentialsId: GIT_CREDENTIALS_ID
                }
            }
        }

         stage('Setup Python') {
            steps {
                // Install Python virtual environment and upgrade pip
                bat """
                C:\\Users\\User\\AppData\\Local\\Programs\\Python\\Python310\\python.exe -m venv venv
                C:\\Users\\User\\AppData\\Local\\Programs\\Python\\Python310\\python.exe -m pip install --upgrade pip
                """
            }
        }

        stage('Install Dependencies') {
            steps {
                // Install necessary Python packages
                bat """
                C:\\Users\\User\\AppData\\Local\\Programs\\Python\\Python310\\python.exe -m pip install -r requirements.txt
                C:\\Users\\User\\AppData\\Local\\Programs\\Python\\Python310\\python.exe -m pip install httpx
                C:\\Users\\User\\AppData\\Local\\Programs\\Python\\Python310\\python.exe -m pip install python-multipart
                """
            }
        }

        stage('Run uvicorn in background') {
            steps {
                script {
                    // Use nohup to run uvicorn in the background
                    bat 'start cmd /c "uvicorn main:app --reload"'

                    // Wait for a few seconds to ensure uvicorn starts successfully
                    sleep 10

                    // You can add additional steps or tests here if needed
                }
            }
        }

        stage('Run Tests') {
            steps {
                // Run the tests
                bat """
                C:\\Users\\User\\AppData\\Local\\Programs\\Python\\Python310\\python.exe -m pip install bcrypt
                C:\\Users\\User\\AppData\\Local\\Programs\\Python\\Python310\\python.exe -m unittest discover tests
                """
            }
        }

    }

    post {
        always {
            // Дії, які виконуються завжди після завершення пайплайну
            echo 'Cleaning up...'
            // Наприклад, видалення тимчасових файлів або артефактів
            // sh 'rm -rf target'
        }
        success {
            // Дії, які виконуються у випадку успішного виконання пайплайну
            echo 'Pipeline succeeded!'
        }
        failure {
            // Дії, які виконуються у випадку помилки виконання пайплайну
            echo 'Pipeline failed!'
        }
    }
}
