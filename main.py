from typing import Annotated
from fastapi import FastAPI, Depends
from starlette import status
import auth
from database import puzzles
from generator import generateData
from models import MyRequest
from processData import Matrix
from auth import get_current_user, check_user

app = FastAPI(
    title="Task22",
    version="0.0.1",
)
app.include_router(auth.router)
user_dependency = Annotated[dict, Depends(get_current_user)]


@app.get("/", status_code=status.HTTP_200_OK)
async def user(user: user_dependency):
    await check_user(user)
    return {"user": user}


@app.get("/get-result/{id_item}")
async def get_data_and_result_from_database(id_item: int, user: user_dependency):
    await check_user(user)
    result = puzzles.find_one({"id_item": id_item})
    return result


@app.get("/generate")
async def get_generated_data(user: user_dependency):
    await check_user(user)
    pipeline = [
        {"$group": {"_id": None, "max_id": {"$max": "$id"}}}
    ]
    result = list(puzzles.aggregate(pipeline))
    max_id = result[0]["max_id"]
    result = generateData(max_id)
    puzzles.insert_one(result)
    return result


@app.post("/process_data/")
async def process_data(request: MyRequest, user: user_dependency):
    await check_user(user)
    matrix = Matrix()
    if matrix.initializeVariables(request=request):
        startMatrix, result = matrix.processData()
        return {"matrix": startMatrix, "result": result}
    else:
        return {"message": "Entered Data Is INVALID"}
