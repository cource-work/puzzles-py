from typing import List, Optional
from pydantic import BaseModel


class Node(BaseModel):
    i: int
    j: int
    value: int

    def printInfo(self):
        print(self.i, self.j, self.value)


class Condition(BaseModel):
    x1: int
    y1: int
    x2: int
    y2: int
    sum: int

    def printInfo(self):
        print(self.x1, self.y1, self.x2, self.y2, self.sum)


class MyRequest(BaseModel):
    n: int
    m: int
    digits: List[Node]
    conditions: List[Condition]


class User(BaseModel):
    username: str
    password: str

