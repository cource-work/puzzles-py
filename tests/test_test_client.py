import unittest
from unittest.mock import patch, Mock
from testClient import send_request


class TestSendRequest(unittest.TestCase):

    @patch('requests.post')
    @patch('builtins.print')
    def test_send_request_success(self, mock_print, mock_post):
        mock_response = Mock()
        mock_response.status_code = 200
        mock_response.json.return_value = {"message": "Success"}
        mock_post.return_value = mock_response

        data = {
            "n": 2,
            "m": 2,
            "digits": [
                {"i": 0, "j": 0, "value": 1},
                {"i": 0, "j": 1, "value": 3}
            ],
            "conditions": []
        }
        send_request(data)

        mock_post.assert_called_once_with(
            "http://127.0.0.1:5049/process_data/",
            headers={"Content-Type": "application/json"},
            data='{"n": 2, "m": 2, "digits": [{"i": 0, "j": 0, "value": 1}, {"i": 0, "j": 1, "value": 3}], "conditions": []}'
        )

        mock_print.assert_called_with('{\n    "message": "Success"\n}')


if __name__ == '__main__':
    unittest.main()
