import unittest
from datetime import timedelta, datetime
from unittest.mock import MagicMock

from jose import jwt
from starlette.testclient import TestClient

from auth import authenticate_user, create_access_token, SECRET_KEY, ALGORITHM, Token, CreateUserRequest, \
    bcrypt_context
from main import app


class TestAuth(unittest.TestCase):

    def test_authenticate_user(self):
        # Mocking the users collection
        mock_user = {
            'username': 'string',
            # Generate a valid bcrypt hash for the password
            'password': bcrypt_context.hash('string')
        }
        mock_find_one = MagicMock(return_value=mock_user)

        # Mocking the users collection find_one method
        users = MagicMock()
        users.find_one = mock_find_one

        # Test with valid username and password
        result = authenticate_user('string', 'string')
        self.assertEqual(result['username'], mock_user['username'])

        # Test with invalid password
        result = authenticate_user('string', 'wrong_password')
        self.assertFalse(result)

        # Test with non-existing user
        users.find_one.return_value = None
        result = authenticate_user('non_existing_user', 'password')
        self.assertFalse(result)

    def test_create_access_token(self):
        username = "test_user"
        expires_delta = timedelta(minutes=15)
        token = create_access_token(username, expires_delta)
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        self.assertEqual(payload["sub"], username)
        self.assertIn("exp", payload)

        expected_expires = datetime.utcnow() + expires_delta
        actual_expires_rounded = datetime.utcfromtimestamp(payload["exp"]).replace(microsecond=0)

        self.assertEqual(actual_expires_rounded, expected_expires.replace(microsecond=0))

    def test_token_model(self):
        token = Token(access_token="test_access_token", token_type="Bearer")
        self.assertEqual(token.access_token, "test_access_token")
        self.assertEqual(token.token_type, "Bearer")

    def test_create_user_request_model(self):
        request = CreateUserRequest(username="test_user", password="test_password")
        self.assertEqual(request.username, "test_user")
        self.assertEqual(request.password, "test_password")

    def test_login_for_access_token(self):
        # Create a test client
        client = TestClient(app)

        # Mock the authenticate_user function to return a dummy user
        def mock_authenticate_user(username, password):
            return {"username": "string"}

        app.dependency_overrides[authenticate_user] = mock_authenticate_user

        # Make a POST request to the login_for_access_token endpoint
        response = client.request("POST", "/auth/token", data={"username": "string", "password": "string"})
        # Verify that the response status code is 200 OK
        self.assertEqual(response.status_code, 200)

        # Verify that the response contains the expected access_token and token_type
        self.assertIn("access_token", response.json())
        self.assertIn("token_type", response.json())
        self.assertEqual(response.json()["token_type"], "bearer")


if __name__ == '__main__':
    unittest.main()
