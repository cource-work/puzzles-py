import unittest
from unittest.mock import patch
from generator import generateData


class TestGenerateData(unittest.TestCase):

    @patch('generator.random.randint')
    def test_generateData(self, mock_randint):
        mock_randint_values = [2, 2, 0, 0, 1, 0, 1, 3]
        mock_randint.side_effect = mock_randint_values

        result = generateData(1)

        expected_result = {
            "n": 2,
            "m": 2,
            "digits": [
                {"i": 0, "j": 0, "value": 1},
                {"i": 0, "j": 1, "value": 3},
            ],
            "conditions": [],
            "id_item": 2,
            "solving": [[1, 3], [3, 3]],
            "reason": ""
        }

        self.assertEqual(result["conditions"], expected_result["conditions"])
        self.assertEqual(result["n"], expected_result["n"])
        self.assertEqual(result["m"], expected_result["m"])
        self.assertEqual(result["digits"], expected_result["digits"])
        self.assertEqual(result["id_item"], expected_result["id_item"])
        self.assertEqual(result["solving"], expected_result["solving"])
        self.assertEqual(result["reason"], expected_result["reason"])

        # Test that all elements in 'digits' are unique
        unique_values = {digit["value"] for digit in result["digits"]}
        self.assertEqual(len(unique_values), len(result["digits"]))


if __name__ == '__main__':
    unittest.main()
