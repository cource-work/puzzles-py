import unittest
from pymongo import MongoClient


class TestMongoDBInsert(unittest.TestCase):

    def setUp(self):
        # Set up the MongoDB URI and connect to the database
        self.mongodb_uri = "mongodb+srv://strelchenko2010amg:ojKwHwbQ27SRB48K@cluster0.d3kbadw.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0"
        self.client = MongoClient(self.mongodb_uri)
        self.db = self.client['strelchenko2010']

    def tearDown(self):
        self.client.close()

    def test_insert_data(self):
        # Test inserting data into the puzzles collection
        data = {"id": 1, "name": "test puzzle"}
        self.db.puzzles.insert_one(data)
        result = self.db.puzzles.find_one({"id": 1})
        self.assertEqual(result['id'], data['id'])
        self.assertEqual(result['name'], data['name'])

        # Test inserting data into the users collection
        user_data = {"username": "test_user", "password": "test_password"}
        self.db.users.insert_one(user_data)
        user_result = self.db.users.find_one({"username": "test_user"})
        self.assertEqual(user_result['username'], user_data['username'])
        self.assertEqual(user_result['password'], user_data['password'])
        self.db.users.delete_many({"username": "test_user"})


if __name__ == '__main__':
    unittest.main()
