import unittest
from datetime import timedelta
from unittest.mock import patch

from fastapi.testclient import TestClient

from auth import create_access_token
from main import app
from models import MyRequest, User
from processData import Matrix

client = TestClient(app)


class TestMain(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.user_data = User(username="test_user", password="test_password")
        cls.access_token = create_access_token("test_user", timedelta(minutes=15))

    def test_not_auth_user(self):
        response = client.get("/")
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'detail': 'Not authenticated'})

    def test_auth_user(self):
        response = client.get("/", headers={"Authorization": f"Bearer {self.access_token}"})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"user": {"username": self.user_data.username}})

    def test_get_process_data_with_invalid_input(self):
        expected_result = "{\"message\":\"Entered Data Is INVALID\"}"
        mock_request = MyRequest(
            n=-2,
            m=2,
            digits=[],
            conditions=[]
        )
        matrix = Matrix()
        with patch.object(matrix, 'initializeVariables', return_value=False):
            response = client.post("/process_data", json=mock_request.dict(),
                                   headers={"Authorization": f"Bearer {self.access_token}"})

            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.text, expected_result)


if __name__ == '__main__':
    unittest.main()
