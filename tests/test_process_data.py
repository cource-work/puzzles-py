import unittest
from unittest.mock import patch
from processData import validateData, Matrix
from models import Node, MyRequest, Condition
from io import StringIO
import sys


class TestProcessData(unittest.TestCase):
    def test_validateData_valid_data(self):
        mock_request = MyRequest(
            n=3,
            m=3,
            digits=[
                Node(i=0, j=0, value=1),
                Node(i=1, j=1, value=2),
                Node(i=2, j=2, value=3)
            ],
            conditions=[]
        )
        result = validateData(mock_request)
        self.assertTrue(result)

    def test_validateData_invalid_data(self):
        mock_request = MyRequest(
            n=2,
            m=2,
            digits=[
                Node(i=0, j=0, value=1),
                Node(i=2, j=2, value=-1)
            ],
            conditions=[]
        )
        result = validateData(mock_request)
        self.assertFalse(result)

    def test_validateData_empty_data(self):
        mock_request = MyRequest(
            n=0,
            m=0,
            digits=[],
            conditions=[]
        )
        result = validateData(mock_request)
        self.assertFalse(result)

    @patch('processData.validateData', return_value=True)
    def test_initializeVariables_valid_data(self, mock_validateData):
        mock_request = MyRequest(
            n=3,
            m=3,
            digits=[
                Node(i=0, j=0, value=1),
                Node(i=1, j=1, value=2),
                Node(i=2, j=2, value=3)
            ],
            conditions=[]
        )
        matrix = Matrix()
        result = matrix.initializeVariables(mock_request)
        self.assertTrue(result)
        self.assertEqual(matrix.matrixNodes, mock_request.digits)
        self.assertEqual(matrix.matrixLength, mock_request.n)
        self.assertEqual(matrix.matrixHeight, mock_request.m)
        self.assertEqual(matrix.conditions, mock_request.conditions)

    @patch('processData.validateData', return_value=False)
    def test_initializeVariables_invalid_data(self, mock_validateData):
        mock_request = MyRequest(
            n=-3,
            m=3,
            digits=[
                Node(i=0, j=0, value=1),
                Node(i=1, j=1, value=2),
                Node(i=2, j=2, value=3)
            ],
            conditions=[]
        )
        matrix = Matrix()
        result = matrix.initializeVariables(mock_request)
        self.assertFalse(result)
        self.assertEqual(matrix.matrixLength, 0)
        self.assertEqual(matrix.matrixHeight, 0)
        self.assertIsNone(matrix.matrixNodes)
        self.assertIsNone(matrix.conditions)

    def test_create_matrix(self):
        matrix = Matrix(2, 2, [
            Node(i=0, j=0, value=1),
            Node(i=0, j=1, value=2)
        ], [])
        matrix.createMatrix()
        self.assertEqual(matrix.matrixNodes, [Node(i=0, j=0, value=1), Node(i=0, j=1, value=2)])
        self.assertEqual(matrix.matrixLength, 2)
        self.assertEqual(matrix.matrixHeight, 2)
        self.assertEqual(matrix.conditions, [])

    @patch.object(Matrix, 'fillMatrix')
    def test_render(self, mock_fillMatrix):
        matrix = Matrix(2, 2, [
            Node(i=0, j=0, value=1),
            Node(i=0, j=1, value=2)
        ], [])
        mock_fillMatrix.return_value = False
        result = matrix.render(0)
        self.assertFalse(result)
        mock_fillMatrix.return_value = True
        result = matrix.render(0)
        self.assertIsNone(result)

    def test_is_valid_move(self):
        matrix = Matrix(3, 3, [
            Node(i=0, j=0, value=1),
            Node(i=0, j=1, value=3),
            Node(i=2, j=1, value=5)
        ], [])
        matrix.createMatrix()
        visited = [[False] * 3 for _ in range(3)]
        result = matrix.isValidMove(1, 1, 5, visited)
        self.assertTrue(result)

    def test_check_conditions(self):
        matrix = Matrix(3, 3, [
            Node(i=0, j=0, value=1),
            Node(i=0, j=1, value=3),
            Node(i=2, j=1, value=5)
        ], [
            Condition(x1=0, y1=0, x2=1, y2=0, sum=4)
        ])
        matrix.createMatrix()
        result = matrix.checkConditions()
        self.assertTrue(result)

    def test_printMatrix(self):
        matrix = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]
        obj = Matrix(matrix)
        obj.matrix = matrix
        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out
            obj.printMatrix()
            output = out.getvalue().strip()
            expected_output = "1 2 3 \n4 5 6 \n7 8 9"
            self.assertEqual(output, expected_output)
        finally:
            sys.stdout = saved_stdout


if __name__ == '__main__':
    unittest.main()


