from models import MyRequest


def validateData(request: MyRequest):
    for value in request.digits:
        if value.i > request.n - 1 or value.j > request.m - 1 or value.value < 0:
            return False
    return request.n > 0 and request.m > 0 and len(request.digits) > 0


class Matrix:
    matrix = []
    reason = ""

    def __init__(self, matrixLength=0, matrixHeight=0, matrixNodes=None, conditions=None):
        self.matrixNodes = matrixNodes
        self.matrixLength = matrixLength
        self.matrixHeight = matrixHeight
        self.conditions = conditions

    def initializeVariables(self, request: MyRequest = None):
        if validateData(request):
            self.matrixNodes = request.digits
            self.matrixLength = request.n
            self.matrixHeight = request.m
            self.conditions = request.conditions
            return True
        self.reason = "Data is invalid."
        return False

    def processData(self):
        self.createMatrix()

        startMatrix = [row[:] for row in self.matrix]

        if self.render(0):
            return startMatrix, self.matrix
        else:
            self.reason = "The arrangement of numbers or conditions for sum does not allow solving the problem"
            return startMatrix, self.reason

    def createMatrix(self):
        self.matrix = [[0 for _ in range(self.matrixHeight)] for _ in range(self.matrixLength)]
        for node in self.matrixNodes:
            self.matrix[node.i][node.j] = node.value

    def printMatrix(self):
        for i in range(len(self.matrix)):
            for j in range(len(self.matrix[0])):
                print(self.matrix[i][j], end=" ")
            print()
        print()

    def render(self, nodeNumber):
        curNode = self.matrixNodes[nodeNumber]
        visited = [[False] * self.matrixHeight for _ in range(self.matrixLength)]
        visited[curNode.i][curNode.j] = True

        if not self.fillMatrix(curNode.i, curNode.j, nodeNumber, curNode.value, visited):
            return False
        if nodeNumber == len(self.matrixNodes) - 1:
            return True

    def fillMatrix(self, i, j, nodeNumber, counter, visited):
        value = self.matrixNodes[nodeNumber].value
        if counter > 1:
            if j - 1 >= 0 and self.matrix[i][j - 1] == 0 and self.isValidMove(i, j - 1, value, visited):
                self.matrix[i][j - 1] = value
                counter -= 1
                visited[i][j - 1] = True
                if not self.fillMatrix(i, j - 1, nodeNumber, counter, visited):
                    self.matrix[i][j - 1] = 0
                    counter += 1
                    visited[i][j - 1] = False
            if i - 1 >= 0 and self.matrix[i - 1][j] == 0 and self.isValidMove(i - 1, j, value, visited):
                self.matrix[i - 1][j] = value
                counter -= 1
                visited[i - 1][j] = True
                if not self.fillMatrix(i - 1, j, nodeNumber, counter, visited):
                    self.matrix[i - 1][j] = 0
                    counter += 1
                    visited[i - 1][j] = False
            if j + 1 < self.matrixLength - 1 and self.matrix[i][j + 1] == 0 and self.isValidMove(i, j + 1, value,
                                                                                                 visited):
                self.matrix[i][j + 1] = value
                counter -= 1
                visited[i][j + 1] = True
                if not self.fillMatrix(i, j + 1, nodeNumber, counter, visited):
                    self.matrix[i][j + 1] = 0
                    counter += 1
                    visited[i][j + 1] = False
            if i + 1 < self.matrixHeight - 1 and self.matrix[i + 1][j] == 0 and self.isValidMove(i + 1, j, value,
                                                                                                 visited):
                self.matrix[i + 1][j] = value
                counter -= 1
                visited[i + 1][j] = True
                if not self.fillMatrix(i + 1, j, nodeNumber, counter, visited):
                    self.matrix[i + 1][j] = 0
                    counter += 1
                    visited[i + 1][j] = False
            return False
        if nodeNumber == len(self.matrixNodes) - 1:
            return True
        if counter <= 1:
            if self.checkConditions():
                self.render(nodeNumber + 1)
            else:
                return False

    def isValidMove(self, row, col, number, visited):
        if row - 1 >= 0 and self.matrix[row - 1][col] == number and not visited[row - 1][col]:
            return False
        if col + 1 < self.matrixLength - 1 and self.matrix[row][col + 1] == number and not visited[row][col + 1]:
            return False
        if row + 1 < self.matrixHeight - 1 and self.matrix[row + 1][col] == number and not visited[row + 1][col]:
            return False
        if col - 1 >= 0 and self.matrix[row][col - 1] == number and not visited[row][col - 1]:
            return False
        return True

    def checkConditions(self):
        for condition in self.conditions:
            curSum = 0
            for i in range(condition.y1, condition.y2 + 1):
                for j in range(condition.x1, condition.x2 + 1):
                    curSum += self.matrix[i][j]
            if curSum == condition.sum:
                return True
            else:
                return False
